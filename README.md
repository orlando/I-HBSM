# HBSM interpretation scripting 

This project is a collection of scripts related to the tt+hf HBSM analysis. The currsnt scripts are described in the following 

### checkout and set up

git clone ssh://git@gitlab.cern.ch:7999/orlando/I-HBSM.git 

go in the directory 

    setupATLAS

    rcSetup Base,2.4.24


### MVA blinding stidies and performance 

To run go under I-HBSM-utils/macros and just type 

    root blinding_study.C 
    
This script evaluate S/B etc, split region by region and for all classidiers. All is currently herd-coded, can be adapted for other studies (e.g meff vs BDT performance). 

### HBSM cross sections 

To run go under I-HBSM-utils/macros and just type 

    root thdm_cross_sections.C


Package to analyse the following inputs

    https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsBSM2HDMRecommendations


Other Benchmarks are here 

    https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGMSSMNeutral
    https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGMSSMCharged
    https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGBRs
    https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HiggsBSM2HDMRecommendations


